Site web
========

## Mise à jour du site Web

- [ ] Mise à jour de la section « /equipe/ »
- [ ] Faire un tour pour vérifier le contenue, le mettre à jour
- [ ] Ajouter informations sur les « Trusted Organizations »
  - [ ] Ajouter lien vers [Prospective Trusted Organizations - (FFIS), Debian France](https://lists.debian.org/debian-project/2014/03/msg00012.html)
  - [ ] Ajouter lien vers [Organizations holding assets in trust for Debian ](https://wiki.debian.org/Teams/Treasurer/Organizations)

## Intégration sur Salsa

- [ ] Mise en place d'un webhook sur le serveur pour mettre à jour le site ?